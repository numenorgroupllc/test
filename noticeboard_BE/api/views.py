from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json
from django.http import HttpResponse, JsonResponse

from api.models import Notice

def index(request):
    print("222222")
    notices = Notice.objects.all()
    print(notices[0])
    data_json = json.loads(serializers.serialize('json', notices))

    all_data = []
    for item in notices:
      print(item.file)
      if item.file.name:
        obj = {
          'content': item.content,
          'exp_date': item.exp_date,
          'project_list': item.project_list,
          'permission': item.permission,
          'url': item.url,
          'file': {
            'path': item.file.path,
            'name': item.file.name,
          }
        }
      else:
        obj = {
          'content': item.content,
          'exp_date': item.exp_date,
          'project_list': item.project_list,
          'permission': item.permission,
          'url': item.url,
          'file': {
            'path': "",
            'name': "",
          }
        }
      all_data.append(obj)

    print(all_data)
    result = {
      'status': 200,
      'notices': all_data
    }
    return JsonResponse(result)

@csrf_exempt
def newNotice(request):
    print("1111111111")
    print(len(request.FILES))
    content = request.POST.get('content')
    url = request.POST.get('url')
    date = request.POST.get('date')
    project_list = request.POST.get('project_list')

    notice = Notice(content=content, url=url, exp_date=date, project_list=project_list)
    print("8888888888888")
    if len(request.FILES) > 0:
      print("xxxxxx")
      notice.file = request.FILES['file']

    notice.save()

    data = {
      'status': 200,
      'message': "Created!",
    }
    return JsonResponse(data)
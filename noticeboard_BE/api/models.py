from django.db import models

# Create your models here.
class Notice(models.Model):
  content = models.CharField(max_length=225)
  exp_date = models.DateField()
  project_list = models.CharField(max_length=225)
  permission = models.CharField(max_length=225)
  file = models.FileField(upload_to='documents/')
  url = models.CharField(max_length=225)

  def __str__(self):
    return self.content
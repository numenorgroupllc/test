from django.urls import path

from . import views

urlpatterns = [
    path('new', views.newNotice, name='newNotice'),
    path('', views.index, name='index'),
]